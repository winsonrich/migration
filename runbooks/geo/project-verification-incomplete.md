# Project verification incomplete

## First and foremost

*Don't Panic*

## Diagnose errors

Check the admin dashboard on the secondary `/admin/geo_nodes` or the
metrics in Grafana.

## Get unverified repositories

```ruby
unverified_registries = Geo::ProjectRegistryFinder.new.find_registries_to_verify(batch_size: 10000).pluck(:id); nil
```

## Verify registries

```ruby
unverified_registries.each do |registry_id|
    Geo::RepositoryVerification::Secondary::SingleWorker.new.perform(registry_id)
end
```

The workers above will use an exclusive lease to prevent another worker from running.
If you want to bypass the exclusive lease, run:

```ruby
unverified_registries = Geo::ProjectRegistryFinder.new.find_registries_to_verify(batch_size: 1000); nil
unverified_registries.each do |registry|
    Geo::RepositoryVerificationSecondaryService.new(registry, :repository)
    Geo::RepositoryVerificationSecondaryService.new(registry, :wiki)
end
```

## Finding exclusive leases for verification workers

```ruby
count = 0

Gitlab::Redis::SharedState.with do |redis|
  cursor = '0'

  loop do
    cursor, keys = redis.scan(
              cursor,
              match: '*geo:repository_verification:secondary:single_worker:*',
              count: 1000
            )

    count += keys.count

    break if cursor == '0'
  end
end

puts "Found #{count}"
```
