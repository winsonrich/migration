# Gitlab Migration Run Books

The aim of these documents is to have a quick guide of what to do to
resolve issues happening before, during, or after the failover.

At some point these, or a subset of these, might need to be moved to
[gitlab.com/gitlab-com/runbooks](https://gitlab.com/gitlab-com/runbooks).

## Geo

* [Resolve repo & wiki sync failures](geo/repo-and-wiki-sync-failures.md)
* [Resolve repo & wiki verification failures on the primary](geo/repo-and-wiki-verification-failures-on-primary.md)
* [Resolve repo & wiki verification failures on the secondary](geo/repo-and-wiki-verification-failures-on-secondary.md)
* [Negative out of sync metrics](geo/negative-out-of-sync-metrics.md)
* [Project verification incomplete](geo/project-verification-incomplete.md)
* [Resolve attachment failures and stuck unsynced](geo/attachment-sync-failures-and-unsynced.md)
* [Manually verify attachments](geo/manually-verify-attachments.md)
* [Synchronize Feature Flags configuration with production environment](geo/synchronize-feature-flags-configuration.md)
